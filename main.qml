import QtQuick 1.0

Rectangle {
	id: main
	width: 800; height: 500

	Image {
		id: field
		source: "soccer-field.png"
		anchors.fill: parent
	}

	Component {
		id: player

		Rectangle {
			id: rect

			property alias number: tNum.text
			property alias label: tLabel.text

			//color: "white"
			gradient : Gradient {
				GradientStop { position: 0.0; color: "red" }
				GradientStop { position: 1.0; color: "black" }
			}

			width: 15; height: 15
			radius: 7

			Text {
				id: tNum
				color: "yellow"
				anchors.horizontalCenter: parent.horizontalCenter
			}

			Text {
				id: tLabel
				color: "black"
				anchors {
					top: tNum.bottom
					horizontalCenter: parent.horizontalCenter
				}
			}

			MouseArea {
				anchors.fill: parent
				drag.target: rect
				drag.axis: Drag.XandYAxis
				drag.minimumX: 0
				drag.minimumY: 0
				drag.maximumX: parent.parent.parent.width - rect.width
				drag.maximumY: parent.parent.parent.height - rect.height
			}
		}
	}

	Loader { sourceComponent: player; onLoaded: { item.number = "1"; item.label = "goleiro"; item.x = 50; item.y = 250 } }
	Loader { sourceComponent: player; onLoaded: { item.number = "2"; item.label = "lateral D"; item.x = 200; item.y = 400 } }
	Loader { sourceComponent: player; onLoaded: { item.number = "3"; item.label = "central"; item.x = 100; item.y = 200 } }
	Loader { sourceComponent: player; onLoaded: { item.number = "4"; item.label = "zagueiro"; item.x = 100; item.y = 300 } }
	Loader { sourceComponent: player; onLoaded: { item.number = "5"; item.label = "bagre"; item.x = 300; item.y = 250 } }

	Keys.onPressed: {
		if (event.key == Qt.Key_Q) {
			console.log("exiting")
			event.accepted = true
			Qt.quit()
		}
	}

	focus: true
}
